import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Home from '@/components/dashboard/home'
import Users from '@/components/dashboard/users'

Vue.use(Router)

Vue.component('sidebar', resolve => {
  require(['../components/sidebar.vue'], resolve)
})

Vue.component('headerbar', resolve => {
  require(['../components/header.vue'], resolve)
})

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard-home',
      component: Home
    },
    {
      path: '/users',
      component: Users
    }
  ]
})
